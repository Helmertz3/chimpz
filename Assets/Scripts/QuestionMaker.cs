﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionMaker : MonoBehaviour {

    public const int QUICK_QUESTIONS = 0;
    public const int WORLD_QUESTIONS = 1;
    public const int ENVIRONMENT_QUESTIONS = 2;

    public static int questionType = QUICK_QUESTIONS;

    private static Question[] worldQuestions;

    public static Question[] MakeQuestions() {

        if (worldQuestions == null)
        {
            FillQuestions();
        }


        if (questionType == WORLD_QUESTIONS)
        {
            ShuffleQuestions(worldQuestions);
            return worldQuestions;
        }
        else
        {
            ShuffleQuestions(worldQuestions);
            Question[] temp = new Question[3];
            temp[0] = worldQuestions[0];
            temp[1] = worldQuestions[1];
            temp[2] = worldQuestions[2];
            return temp;
        }
    }

    private static void FillQuestions()
    {
        worldQuestions = new Question[10];
        worldQuestions[0] = new Question("Out of all 1 year olds in the world, how many have been vaccinated?", new string[] { "80 %", "50 %", "20%" }, 0);
        worldQuestions[1] = new Question("What % of the world population has some access to electricity?", new string[] { "20 %", "50 %", "80 %" }, 2);
        worldQuestions[2] = new Question("An avarage 30 years old man has gone to school for 10 years. How many years has the avarage 30 years old woman gone to school?", new string[] { "9 years", "6 years", "3 years" }, 0);
        worldQuestions[3] = new Question("What is the life expectancy of the world?", new string[] { "50 years", "60 years", "70 years" }, 2);
        worldQuestions[4] = new Question("During the last 20 years, the amount of people living in extreme poverty has...", new string[] { "Almost doubled", "Stayed the same", "Almost halved" }, 2); // halved
        worldQuestions[5] = new Question("Where does the majority of people in the world live?", new string[] { "Low-income countries", "Middle-income countries", "High-income countries" }, 1); // Middle
        worldQuestions[6] = new Question("What is the percentace of girls in low-income countries that finishes elementary school?", new string[] { "20 %", "40 %", "60 %" }, 2); // 60 %
        worldQuestions[7] = new Question("Tigers, Giant Pandas and Black Rhinos were listed as endangered species 1996. Since then, how many of them have become more endangered?", new string[] { "None of them", "One of them", "Two of them" }, 0);
        worldQuestions[8] = new Question("The climate experts are saying that the avarage temperature on earth will...", new string[] { "Get hotter", "Stay stable", "Get colder" }, 0);
        worldQuestions[9] = new Question("Today there are 2 billion children (0-15 years) in the world. How many will there be in year 2100 according to experts from the UN?", new string[] { "4 billion", "3 billion", "2 billion" }, 2); // 2 billion
    }

    private static void ShuffleQuestions(Question[] questions)
    {
        for (int i = 0; i < questions.Length; i++)
        {
            Question tmp = questions[i];
            int r = Random.Range(i, questions.Length);
            questions[i] = questions[r];
            questions[r] = tmp;
        }
    }

}
 