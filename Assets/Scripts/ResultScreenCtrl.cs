﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultScreenCtrl : MonoBehaviour {



    // Use this for initialization
    void Start()
    {
        List<Result> shittyResults = new List<Result>();
        List<Result> geniusResults = new List<Result>();
        shittyResults = UserResults.Singleton.getShittyResults();
        geniusResults = UserResults.Singleton.getGeniusResults();

        UserResults.Singleton.clear();

        float percent = Mathf.Round((float)(geniusResults.Count*1000) / (geniusResults.Count + shittyResults.Count))/10;

        GameObject.Find("ResultScore").GetComponent<Text>().text = percent + "%";

        if(percent < 30){
            GameObject.Find("ResultText").GetComponent<Text>().text = "You were beaten by the chimps...";
            GameObject.Find("ChimpSad").SetActive(false);
            GameObject.Find("ChimpThinker").SetActive(false);
        }
        else if(percent < 35){
            GameObject.Find("ResultText").GetComponent<Text>().text = "You're about as smart as a chimp!";
            GameObject.Find("ChimpHappy").SetActive(false);
            GameObject.Find("ChimpSad").SetActive(false);
        }
        else{
            GameObject.Find("ResultText").GetComponent<Text>().text = "Great! You beat the chimps!";
            GameObject.Find("ChimpHappy").SetActive(false);
            GameObject.Find("ChimpThinker").SetActive(false);
        }

        string summary = "";

        if (shittyResults.Count != 0)
        {
            summary += "--- Wrong Answers --- \n\n";

            foreach (Result r in shittyResults)
            {

                summary += r.question.GetQuestion() + "\n";
                summary += "   ✖  " + r.question.GetAnswer(r.userAnswer) + "\n";
                summary += "   ✔  " + r.question.GetCorrectAnswer() + "\n\n";

            }
        }
        if (geniusResults.Count != 0)
        {
            summary += "\n --- Correct Answers --- \n\n";

            foreach (Result r in geniusResults)
            {

                summary += r.question.GetQuestion() + "\n";
                summary += "   ✔  " + r.question.GetCorrectAnswer() + "\n\n";

            }
        }
        GameObject.Find("ResultListText").GetComponent<Text>().text = summary;
    }



}
