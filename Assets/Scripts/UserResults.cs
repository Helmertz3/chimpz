﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserResults : MonoBehaviour
{


    List<Result> results;

    public static UserResults Singleton;

    // Use this for initialization
    void Start()
    {

        if (Singleton == null || Singleton == this)
        {
            Singleton = this;
            results = new List<Result>();
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this.gameObject);
        }


    }

    public void addResult(Result result)
    {
        results.Add(result);
    }


    public List<Result> getShittyResults()
    {
        List<Result> wrongResults = new List<Result>();

        foreach (Result r in results)
        {
            if (!r.isCorrect())
            {
                wrongResults.Add(r);
            }
        }
        return wrongResults;

    }

    public List<Result> getGeniusResults()
    {

        List<Result> geniusResults = new List<Result>();

        foreach (Result r in results)
        {
            if (r.isCorrect())
            {
                geniusResults.Add(r);
            }
        }
        return geniusResults;
    }



    public void clear(){
        results = new List<Result>();  
    }

}
