﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnswerCtrl : MonoBehaviour {

    GameObject[] AnimationCheckMarks;
    public GameObject[] toggles;

	// Use this for initialization
	void Start () {
        ClearToggles();

        AnimationCheckMarks = new GameObject[3];
        AnimationCheckMarks[0] = GameObject.Find("CheckmarkDot1");
        AnimationCheckMarks[1] = GameObject.Find("CheckmarkDot2");
        AnimationCheckMarks[2] = GameObject.Find("CheckmarkDot3");
        AnimationCheckMarks[0].SetActive(false);
        AnimationCheckMarks[1].SetActive(false);
        AnimationCheckMarks[2].SetActive(false);

	}

    public void ClearToggles()
    {
        for (int i = 0; i < 3; i++)
        {
            toggles[i].GetComponent<Toggle>().isOn = false;
        }
    }

    public int GetUserAnswer()
    {
        for (int i = 0; i < 3; i++)
        {
            if (toggles[i].GetComponent<Toggle>().isOn)
            {
                return i;
            }
        }
        return -1;
    }
	
    public void OnClick(int pressedToggleIndex)
    {
        if (toggles[pressedToggleIndex].GetComponent<Toggle>().isOn)
        {
            AnimationCheckMarks[pressedToggleIndex].SetActive(true);
            for (int i = 0; i < 3; i++)
            {
                if (pressedToggleIndex != i)
                {
                    toggles[i].GetComponent<Toggle>().isOn = false;
                }
            }
        }
        else{
            AnimationCheckMarks[pressedToggleIndex].SetActive(false);
        }
    }
}
