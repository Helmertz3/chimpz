﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class QuestionCtrl : MonoBehaviour {

    Question[] questions;
    int counter;

    AnswerCtrl answerCtrl;

	// Use this for initialization
	void Start () {
        counter = 0;
        questions = QuestionMaker.MakeQuestions();
        answerCtrl = GameObject.Find("AnswerCtrl").GetComponent<AnswerCtrl>();

        LoadNextQuestion();
	}

    public void LoadNextQuestion()
    {
        GameObject.Find("A1Text").GetComponent<Text>().text = questions[counter].GetAnswer(0);
        GameObject.Find("A2Text").GetComponent<Text>().text = questions[counter].GetAnswer(1);
        GameObject.Find("A3Text").GetComponent<Text>().text = questions[counter].GetAnswer(2);
        GameObject.Find("QText").GetComponent<Text>().text = questions[counter].GetQuestion();
        
    }

    public void OnClick()
    {
        if(answerCtrl.GetUserAnswer() != -1){
            Result resultTemp = new Result(questions[counter], answerCtrl.GetUserAnswer());
            UserResults.Singleton.addResult(resultTemp);
            answerCtrl.ClearToggles();
            counter++;
            if (counter < questions.Length)
            {
                LoadNextQuestion();
            }
            else
            {
                SceneManager.LoadSceneAsync("ResultScreen");
            } 
        }

    }
	
}
