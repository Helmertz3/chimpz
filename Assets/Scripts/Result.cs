﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Result {

    public Question question;
    public int userAnswer;
    
    public Result(Question question, int answer)
    {
        this.question = question;
        userAnswer = answer;
    }
	

    public bool isCorrect(){

        bool yeet;

        if (question.correctAnswer == userAnswer){
            yeet = true;
        }
        else {
            yeet = false;
        }


        return yeet;
    }
}
