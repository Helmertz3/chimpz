﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Question {
    
    string question;
    string[] answers;
    public int correctAnswer;  

	public Question(string question, string[] answers, int correctAnswer)
	{
        this.question = question;
        this.answers = answers;
        this.correctAnswer = correctAnswer;
	}

    public string GetQuestion()
    {
        return question;
    }

    public string GetAnswer(int pos)
    {
        return answers[pos];
    }

    public string GetCorrectAnswer(){
        return answers[correctAnswer];
    }

}
