﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonCtrl : MonoBehaviour {
    
    public void SwitchScene(string sceneName)
    {
        SceneManager.LoadSceneAsync(sceneName);
    }

    public void SwitchToQuestionScene(int type)
    {
        QuestionMaker.questionType = type;
        SwitchScene("QuestionScreen");
    }
}
